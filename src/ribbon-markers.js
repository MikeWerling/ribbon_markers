import './ribbon-markers.css';
const MarkerClusterer = require('./markerclusterer.js');
const mapUtils = require('./googleMapUtils.js');
const markerSymbology = require('./markerSymbology.js');

//Creating a unique ID for the map so we don't have issues with more than one map on a dashboard
let mapDivId = 'map-' + controller.thread.UUID;
let dataFromZoomdata = [];

//We have to embed the Google API call in the web page since we can't load it as a custom
//chart library in the normal (at least normal for Zoomdata) way
let sc = document.createElement('script');
let googleAPIKey = controller.variables['Google API Key'];
sc.src = `https://maps.googleapis.com/maps/api/js?key=${googleAPIKey}&libraries=geometry`;
sc.onload = function() {
  //wait for the Google API to load before trying to create the map
  initMap();
  //Have to handle the async nature of things.  The data from Zoomdata may have come in
  //before the map was ready.  Check and load if needed
  updateMapPoints(dataFromZoomdata);
};
document.head.appendChild(sc);

//Create a div to hold the map
const chartContainer = document.createElement('div');
chartContainer.id = mapDivId;
chartContainer.style.width = '100%';
chartContainer.style.height = '100%';
chartContainer.classList.add('chart-container');

controller.element.appendChild(chartContainer);
let googleMap;
let markerCluster; //object containing all markers to be displayed, using a point clustering scheme to cut down on display clutter
let mapOverlay;

//InfoWindow is the Google Maps style tooltip.  TODO: are we using this or Zoomdata's?
let infoWindow;

function createClusterStyles() {
  let iconWidth = 15;
  let newStyles = [];
  let newIcon = markerSymbology.createClusterIcon(iconWidth);
  newStyles.push(newIcon);
  return newStyles;
}

function initMap() {
  let centerPoint = {
    lat: controller.variables['Center Latitude'],
    lng: controller.variables['Center Longitude'],
  };

  googleMap = new window.google.maps.Map(document.getElementById(mapDivId), {
    center: centerPoint,
    zoom: controller.variables['Initial Zoom'],
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    streetViewControl: false,
    mapTypeControl: false,
  });

  markerCluster = new MarkerClusterer(googleMap, [], {
    imagePath:
      'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', //this is actuall ignored with the revised cluster lib to use SVG directly
    styles: createClusterStyles(),
  });

  infoWindow = new google.maps.InfoWindow({
    content: 'Blank',
  });

  //if we are displaying the error radius in actual meters we need to
  //redraw the symbols each time the user zooms.  The number of pixels depends on
  //the zoom level
  googleMap.addListener('zoom_changed', function() {
    var currentZoom = googleMap.getZoom();
    console.log('Zoom level:', currentZoom);
    markerCluster.getMarkers().forEach(marker => {
      marker.setIcon(
        markerSymbology.getCircle(
          marker.zd_properties.metric,
          marker.zd_properties.error,
          googleMap,
        ),
      );
    });
  });
}

// This is where most of the work gets done.
// Create a marker for each data row.  Each row of group data comes in an array, so order is important. The order is based on the way the
//multi-group variable was declared, in our case it should be [id, lat, lon]
// We create a marker for each row.  Stash additional properties in each marker object to record the metric value, error value, and
// the original Zoomdata data object (which is used for the radial menu and the tooltip)
function updateMapPoints(data) {
  //Some counters to help with debugging and understanding how this works
  let newPointsCount = 0;
  let updatedPointsCount = 0;
  let deletedPointsCount = 0;
  let markersToDelete = []; //track a list of markers that are no longer in the data so we can remove them
  //First we need to update any markers currently on the screen with a matching ID, otherwise we end up with duplicate points for each ID
  markerCluster.getMarkers().forEach(marker => {
    //find the data by ID and update if present.  Remove it from the data collection,
    //we need the leftovers which are new points.
    let idx = _.findIndex(data, element => {
      return element.group[0] === marker.zd_properties.id;
    });
    if (idx !== -1) {
      //Update feature cooridinates and properties
      let newPoint = new google.maps.LatLng(
        data[idx].group[1],
        data[idx].group[2],
      );
      let metric = getMetricValue(data[idx].current, 'Color');
      let error = getMetricValue(data[idx].current, 'Error');
      //      feature.setGeometry(newPoint);
      marker.zd_properties.metric = metric;
      marker.zd_properties.error = error;
      //also generate a new symbol in case the values changed
      marker.setIcon(markerSymbology.getCircle(metric, error, googleMap));
      //remove from the data collection so we know the feature was found and updated
      data.splice(idx, 1);
      updatedPointsCount++;
    } else {
      //We have a map feature with no corresponding data element from Zoomdata
      //flag it for deletion, not in the data collection
      //We aren't doing the delete here, as it would change the marker collection that we are looping on,
      //so results would be inconsistent
      markersToDelete.push(marker);
    }
  });
  markersToDelete.forEach(marker => {
    markerCluster.removeMarker(marker);
    deletedPointsCount++;
  });

  //any rows left in data need to be added to the map, they aren't existing features
  let newMarkers = [];
  data.forEach(row => {
    //Depending on how the Zoomdata source is configured the coordinates could be coming in as a number or a string.  If string make sure to convert them
    let lat;
    let lon;
    if (typeof row.group[1] === 'string') {
      lat = parseFloat(row.group[1]);
    } else {
      lat = row.group[1];
    }
    if (typeof row.group[2] === 'string') {
      lon = parseFloat(row.group[2]);
    } else {
      lon = row.group[2];
    }
    let metric = getMetricValue(row.current, 'Color');
    let error = getMetricValue(row.current, 'Error');
    let newMarker = new google.maps.Marker({
      position: { lat: lat, lng: lon },
      icon: markerSymbology.getCircle(metric, error, googleMap),
      zd_properties: {
        id: row.group[0],
        metric: metric,
        error: error,
        data: row,
      },
    });

    newMarker.addListener('mouseover', evt => {
      //the event parameter is just the mouse event, does not actually contain a reference to the marker that was clicked on, unlike
      // using a raw data point on the data layer (or the way most any other geo library would work).  We are pre-building the info window
      // using the current values.  Fun.
      //
      // Initially I used evt.Ia.pageX, but that changed when I rebuilt in Jun, now the variable is
      // Ha.  Some compile/minification process when they build?  The pageX/pageY aren't exposed
      // through the Google API as properties/methods, so added some logic to find the attribute
      // containing the coordinates dynamically

      let pageX = 0;
      let pageY = 0;

      Object.keys(evt).forEach(key => {
        if (
          typeof evt[key] !== 'undefined' &&
          typeof evt[key].pageX !== 'undefined'
        ) {
          pageX = evt[key].pageX;
          pageY = evt[key].pageY;
        }
      });
      controller.tooltip.show({
        x: pageX,
        y: pageY,
        data: () => {
          return newMarker.zd_properties.data;
        },
      });
    });

    newMarker.addListener('mouseout', evt => {
      controller.tooltip.hide();
    });

    //When the user clicks on a marker show the Zoomdata radial menu.
    newMarker.addListener('click', evt => {
      controller.tooltip.hide();
      //see tooltip for description of why we are searching for page coordinates
      let pageX = 0;
      let pageY = 0;

      Object.keys(evt).forEach(key => {
        if (
          typeof evt[key] !== 'undefined' &&
          typeof evt[key].pageX !== 'undefined'
        ) {
          pageX = evt[key].pageX;
          pageY = evt[key].pageY;
        }
      });
      controller.menu.show({
        x: pageX,
        y: pageY,
        data: function() {
          return newMarker.zd_properties.data;
        },
        menu: {
          items: ['Filter', 'Filter All', 'Unfilter All', 'Info'],
        },
      });
    });
    newMarkers.push(newMarker);
    newPointsCount++;
  });

  markerCluster.addMarkers(newMarkers);
  markerCluster.repaint();
  console.log(
    `On update: newPoints: ${newPointsCount} deletedPoints: ${deletedPointsCount} updatedPoints: ${updatedPointsCount}, total number of markers: ${markerCluster.getTotalMarkers()}`,
  );
}

//---- Zoomdata Specific Functions ----
// called when new data is received from server
controller.update = data => {
  dataFromZoomdata = data;
  updateMapPoints(data);
};

// called when the chart widget is resized
controller.resize = (newWidth, newHeight) => {};

//show the picker for color.  We don't show a picker for error, the user cannot change what field
//is used for the error buffer (admin can change it in chart settings)
controller.createAxisLabel({
  picks: 'Color',
  orientation: 'horizontal',
  position: 'bottom',
  popoverTitle: 'Color',
});

//Helper function to get a metric value from Zoomdata data object.
//pass in 'data[i].current' as the data object
function getMetricValue(data, metricVariableName) {
  let metricInfo = controller.dataAccessors[metricVariableName].getMetric();
  //Count/volume is a special case, no function and the value is in a different place
  let result;
  if (metricInfo.name === 'count') {
    result = data.count;
  } else {
    result = data.metrics[metricInfo.name][metricInfo.func];
  }
  return result;
}
