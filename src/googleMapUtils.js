// Some helper functions for use with Google Maps API

let googleMapUtils = {
  //Take a specific pixel on a map and return the underlying geographic coordinates
  pixelToLatLon: function(xcoor, ycoor) {
    var ne = window[mapVarId].getBounds().getNorthEast();
    var sw = window[mapVarId].getBounds().getSouthWest();
    var projection = window[mapVarId].getProjection();
    var topRight = projection.fromLatLngToPoint(ne);
    var bottomLeft = projection.fromLatLngToPoint(sw);
    var scale = 1 << window[mapVarId].getZoom();
    var pointt = new window.google.maps.Point(
      xcoor / scale + bottomLeft.x,
      ycoor / scale + topRight.y,
    );
    var newLatlng = projection.fromPointToLatLng(pointt);
    return newLatlng;
  },

  //Take lat/lon coordinates and return the pixel coordinates on the screen.  We need the coordinates,  map object, and the
  //div to calculate.
  // Note that this is much harder than it should be, other JS Map APIs give a clear function to do this.  Google gives us projected
  //pixels on the map tile, then we have to also convert the upper left corner, then subtract our click point.
  //Returns a google Point object, which is just a pair of coords
  latlonToPixel: function(latLon, googleMap) {
    let numTiles = 1 << googleMap.getZoom();
    console.log('numTiles = ', numTiles);
    let projection = googleMap.getProjection();
    let projectedCoords = projection.fromLatLngToPoint(latLon); //converts to projected tile coordinates
    let pixelCoords = new window.google.maps.Point(
      projectedCoords.x * numTiles,
      projectedCoords.y * numTiles,
    );
    let bounds = googleMap.getBounds();
    let topLeft = new window.google.maps.LatLng(
      bounds.getNorthEast().lat(),
      bounds.getSouthWest().lng(),
    );
    let projectedTopLeft = projection.fromLatLngToPoint(topLeft);
    let topLeftPixels = new window.google.maps.Point(
      projectedTopLeft.x * numTiles,
      projectedTopLeft.y * numTiles,
    );
    let result = new window.google.maps.Point(
      pixelCoords.x - topLeftPixels.x,
      pixelCoords.y - topLeftPixels.y,
    );
    return result;
  },

  //From https://gist.github.com/Xennis/7db1a832250d3e33c65b, convert radius meters to pixels
  //Portions of this core are modifications based on work created and shared
  //by Google (https://developers.google.com/site-policies) and used according
  //to terms described in the Apache 2.0 License
  //(http://www.apache.org/licenses/LICENSE-2.0)

  TILE_SIZE: 256,

  bound: function(value, opt_min, opt_max) {
    if (opt_min !== null) value = Math.max(value, opt_min);
    if (opt_max !== null) value = Math.min(value, opt_max);
    return value;
  },

  degreesToRadians: function(deg) {
    return deg * (Math.PI / 180);
  },

  radiansToDegrees: function(rad) {
    return rad / (Math.PI / 180);
  },

  fromLatLngToPoint: function(latLng, opt_point) {
    let self = this;
    let pixelOrigin_ = new window.google.maps.Point(
      self.TILE_SIZE / 2,
      self.TILE_SIZE / 2,
    );

    var point = opt_point || new window.google.maps.Point(0.0, 0.0);
    var origin = pixelOrigin_;
    let pixelsPerLonDegree_ = self.TILE_SIZE / 360;
    point.x = origin.x + latLng.lng() * pixelsPerLonDegree_;

    // NOTE(appleton): Truncating to 0.9999 effectively limits latitude to
    // 89.189.  This is about a third of a tile past the edge of the world
    // tile.
    var siny = self.bound(
      Math.sin(self.degreesToRadians(latLng.lat())),
      -0.9999,
      0.9999,
    );
    let pixelsPerLonRadian_ = self.TILE_SIZE / (2 * Math.PI);
    point.y =
      origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -pixelsPerLonRadian_;
    return point;
  },

  fromPointToLatLng: function(point) {
    var self = this;
    let pixelOrigin_ = new window.google.maps.Point(
      self.TILE_SIZE / 2,
      self.TILE_SIZE / 2,
    );
    var origin = pixelOrigin_;
    let pixelsPerLonDegree_ = self.TILE_SIZE / 360;
    var lng = (point.x - origin.x) / self.pixelsPerLonDegree_;
    let pixelsPerLonRadian_ = self.TILE_SIZE / (2 * Math.PI);
    var latRadians = (point.y - origin.y) / -pixelsPerLonRadian_;
    var lat = self.radiansToDegrees(
      2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2,
    );
    return new window.google.maps.LatLng(lat, lng);
  },

  //
  // Converts the given radius in meters to pixels.
  //
  // @param {number} desiredRadiusInInMeters Desired radius in meters
  // @returns {number} Desired redius in meters
  //
  getRadiusInPixels: function(map, desiredRadiusInInMeters) {
    let self = this;
    var numTiles = 1 << map.getZoom();
    var center = map.getCenter();
    var moved = window.google.maps.geometry.spherical.computeOffset(
      center,
      10000,
      90,
    ); // 1000 meters to the right
    var initCoord = self.fromLatLngToPoint(center);
    var endCoord = self.fromLatLngToPoint(moved);
    var initPoint = new window.google.maps.Point(
      initCoord.x * numTiles,
      initCoord.y * numTiles,
    );
    var endPoint = new window.google.maps.Point(
      endCoord.x * numTiles,
      endCoord.y * numTiles,
    );
    var pixelsPerMeter = Math.abs(initPoint.x - endPoint.x) / 10000.0;
    var totalPixelSize = Math.floor(desiredRadiusInInMeters * pixelsPerMeter);
    return totalPixelSize;
  },
};

module.exports = googleMapUtils;
