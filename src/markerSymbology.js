const mapUtils = require('./googleMapUtils.js');
/*
Functions to generate icons/symbols for markers.  Generates 2 layer circle for the individual icon, with the 
symbol color representing the metric and the outer circle representing the error.  Also create the symbol for the
clustered markers, where multiple markers are combined into one simple symbol

*/

// Calculate the radius for the 'halo' around the point.  Shouldn't be smaller than the
// main marker point.  Need to figure out an upper limit as well.
//
//error parameter is kilometers radius around the main point
function calculateErrorRadius(error, minRadius, map) {
  let result = mapUtils.getRadiusInPixels(map, error * 1000); //function needs meters, so convert
  if (result < minRadius) result = minRadius;
  return result;
  //TODO: faking out the radius with some random so we see something, change to reflect actual value,
  //      depending on the units and values for the error (meters, kilometers, or some sort of relative value)
  //return Math.floor(Math.random() * (minRadius * 3 - minRadius)) + minRadius;
}

let markerSymbology = {
  /* 
  Construct the icon for the cluster icon.  This is a departure from the stock markercluster method that Google provides,
  the stock method uses PNG images.  We are building raw SVG to create the cluster symbol.  First, Zoomdata can't load images
  locally in the custom visualization, so we would have to host the images on another server (and make sure the client can access).  Second
  this gives us more flexibility for the shape of the icon, provied our svg-fu is up to the task
  */
  createClusterIcon: function(iconWidth) {
    let color = '#FF0000';
    //using a diamond shape, which is a square rotated 90 degrees.  To get the bounding box we need to calculate the diagonal of the square,
    //thanks to Pythagoras
    let boxWidth = Math.sqrt(iconWidth * iconWidth * 2);
    let offset = (boxWidth - iconWidth) / 2; //The origin for the diamond is offset slightly from the top left corner due to rotation of square
    let svg = `data:image/svg+xml;utf-8, \
               <svg width='${boxWidth}' height='${boxWidth}' xmlns='http://www.w3.org/2000/svg'> \
                <rect \
                  x='${offset}' y='${offset}' width='${iconWidth}' height='${iconWidth}' \
                  style='fill:#0022FF;fill-opacity:0.9' \
                  transform='rotate(45, ${boxWidth / 2}, ${boxWidth / 2})'\
                ></rect>\
              </svg>`;

    let anchorPoint = new google.maps.Point(iconWidth, iconWidth); //we have to shift the origin
    return {
      url: svg,
      height: boxWidth,
      width: boxWidth,
      anchor: anchorPoint,
      textColor: '#FFFFFF',
      textSize: iconWidth - 2,
      backgroundPosition: '0,0',
    };
  },

  //return a circle depending on what type of feature it is - color dot or error buffer
  getCircle: function(metric, error, map) {
    const markerRadius = 5; //change this to make color circles bigger/smaller

    let errorRadius = calculateErrorRadius(error, markerRadius, map);

    //errorRadius should never be smaller than the marker (or our SVG math will be all screwed up),
    if (errorRadius < markerRadius) {
      errorRadius = markerRadius;
    }

    let color = controller.dataAccessors.Color.getColor(metric);

    //Building a dynamic SVG object.
    // total width/height is 2*radius (diameter)
    let svg = `data:image/svg+xml;utf-8, \
                <svg width="${2 * errorRadius}" height="${2 *
      errorRadius}" xmlns="http://www.w3.org/2000/svg"> \
                  <path fill="yellow" fill-opacity=".3" stroke="white" stroke-width="1" d="M0,\
                    ${errorRadius} a${errorRadius},${errorRadius} 0 1,0 ${2 *
      errorRadius},0\
                    a${errorRadius},${errorRadius} 0 1,0 -${2 *
      errorRadius},0" > \
                  </path> \
                  <path fill="${color}" stroke="white" stroke-width="1" d="M${errorRadius -
      markerRadius},${errorRadius} \
                    a${markerRadius},${markerRadius} 0 1,0 ${2 *
      markerRadius},0\
                    a${markerRadius},${markerRadius} 0 1,0 -${2 *
      markerRadius},0" > \
                  </path> \
                </svg>`;
    let anchorPoint = new google.maps.Point(errorRadius, errorRadius); //we have to shift the origin over the radius, otherwise we draw off-center
    return {
      url: svg,
      anchor: anchorPoint,
      //   scale: markerRadius,
      //   strokeColor: 'white',
      //   strokeWeight: 0.5,
    };
  },
};

module.exports = markerSymbology;
