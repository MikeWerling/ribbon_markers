# Ribbon Markers

Custom chart build specific ally for Ribbon to display points loaded from Zoomdata.

Built for Zoomdata 2.6.x on contract to Ribbon to display a map showing attributes and associated metrics
from the Zoomdata source.

Prerequisites:
* Each entity in the data has a unique ID, recorded as a field in the data rows
* Each row of the data source must contain a latitude and longitude
* The latitude and longitude are attributes using
the source configuration to change the column type, since coordinates are usually stored as numbers
* Each row has an error value, which is the number of meters the location (lat/lon) could be incorrect

The chart:
1. Uses the Google Maps API (per Ribbon requirement)
2. Uses a multi-group by query on latitude, longitude, ID.  This is the only way we have found to map data
where the rows contain lat/lon and still perform aggregation (standard Map: Markers chart uses raw data query)
3. Display a point for each row returned from the query
4. Zoom level based clustering of points
5. Each point colored based on the aggregated metric
6. An additional circle drawn around each point to represent the error
7. Zoomdata radial menu
8. Support standard Zoomdata functions (filter, sort, color)

The code uses the Google API with the Marker Cluster add-on.  The clustering code was customized to allow drawing the symbols using SVG instead of PNG.

## Building
This chart was built for Zoomdata 2.6.x. Requires the Zoomdata chart command line interface (CLI), currently uses version 3.0.0 of CLI.  Follow the CLI instructions (https://www.npmjs.com/package/zoomdata-chart-cli) to install and configure the CLI. 

This chart uses webpack, so that will need to be installed using 
`npm install -g webpack`
`npm install -g webpack-cli`

Pull the code, do `npm install`

This project uses webpack to build the bundled file to import into Zoomdata.  
* To change the visualization edit the files in the `src` directory.  `src/ribbon-markers` is the main source file, with the others providing supporting functions
* Run `npm run build` to compile the project.

If building the visualization from source, without it existing on the server already or having a zip import file, you have to manually create the zip import file:
`zip -r ribbon-markers.zip version visualization.json components`

Then use the chart CLI to add the chart to Zoomdata
`zd-chart push 'Ribbon Markers' ribbon-markers.zip`

After the chart has intially been added to Zoomdata any changes need to be built and pushed to the server.
`npm run build`
`zd-chart push`

Or you can set up a set of watchers to monitor the code and automatically push changes.  Open two terminal windows to the folder containing the code.  In one run
`npm start` will start webpack in developer mode, watching the files
In the other window run
`zd-chart watch` which will watch the files in the `components` folder for changes and automatically push them to Zoomdata

  


## Zoomdata 3.x
 This visualization will need to go through the migration process for moving to Zoomdata 3.x, althought since it uses webpack already that process may be easier.